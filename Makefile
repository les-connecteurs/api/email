.PHONY: generate
generate:
	bazel run //:gazelle
	bazel run //:images

.PHONY: start run dev
start:
	ibazel --run_output_interactive=false run //hack:docker-compose -- up -d
run: start
dev: start

.PHONY: logs
logs:
	bazel run //hack:docker-compose -- logs

.PHONY: stop
stop:
	bazel run //hack:docker-compose -- down

.PHONY: update
update:
	go mod tidy
	bazel run //:gazelle -- update-repos -prune -from_file go.mod -to_macro repositories.bzl%go_repositories

.PHONY: upgrade
upgrade:
	go mod tidy
	go get -u ./...
	bazel run //:gazelle -- update-repos -prune -from_file go.mod -to_macro repositories.bzl%go_repositories

.PHONY: clean
clean:
	bazel clean
