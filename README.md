# Email API

## Migrations

Migrations will create and maintain required PostgreSQL tables, views and schemas.

## Sync

This component is used to synchronize data from the Kratos identities endpoint.

## License

This program is free software and is distributed under [AGPLv3+ License](./LICENSE).
