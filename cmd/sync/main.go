package main

import (
	"context"
	"encoding/json"
	"log"
	"net/http"
	"time"

	"connecteu.rs/api/email/utils"
)

var httpClient = &http.Client{
	Timeout: 10 * time.Second,
}

// IdentitiesResponse contains the list of all identities.
type IdentitiesResponse []struct {
	ID     string `json:"id"`
	Traits struct {
		Email string `json:"email"`
	} `json:"traits"`
}

func getJSON(url string, target interface{}) error {
	r, err := httpClient.Get(url)
	if err != nil {
		log.Fatal(err)
	}
	defer r.Body.Close()

	return json.NewDecoder(r.Body).Decode(target)
}

func main() {
	log.Println("Email users sync service started…")

	url := utils.GetEnv("IDENTITIES_ENDPOINT", "http://localhost:4434/identities")
	res := IdentitiesResponse{}
	getJSON(url, &res)

	nb := len(res)

	if nb < 1 {
		log.Fatal("no user returned from identities endpoint")
	}

	log.Println("found", nb, "users")

	db, err := utils.DatabaseInstance()
	if err != nil {
		log.Fatal(err)
	}

	ctx := context.Background()
	tx, err := db.BeginTx(ctx, nil)
	if err != nil {
		log.Fatal(err)
	}

	// delete all users, rollback on failure
	_, err = tx.ExecContext(ctx, "DELETE FROM public.users")
	if err != nil {
		tx.Rollback()
		log.Fatal(err)
	}

	for _, identity := range res {
		log.Print("found user #", identity.ID, " with email ", identity.Traits.Email, "\n")

		// insert user, rollback on failure
		_, err = tx.ExecContext(ctx, "INSERT INTO public.users (user_id, email) VALUES ($1, $2)", identity.ID, identity.Traits.Email)
		if err != nil {
			tx.Rollback()
			log.Fatal(err)
		}
	}

	err = tx.Commit()
	if err != nil {
		log.Fatal(err)
	}

	log.Println("success!")
}
