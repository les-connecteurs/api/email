-- manage aliases
CREATE TABLE IF NOT EXISTS public."aliases" (
  "id"          SERIAL NOT NULL PRIMARY KEY,
  "alias"       TEXT NOT NULL,
  "destination" TEXT NOT NULL
);

-- users (need to be sync)
CREATE TABLE IF NOT EXISTS public."users" (
  "user_id"     TEXT NOT NULL PRIMARY KEY,
  "email"       TEXT NOT NULL
);

-- list of mailboxes (used by Postfix)
CREATE OR REPLACE VIEW public."mailboxes" AS
  SELECT DISTINCT "email"
    FROM public."users";
