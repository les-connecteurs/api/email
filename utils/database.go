package utils

import (
	"database/sql"
	"fmt"

	// postgreSQL support
	_ "github.com/lib/pq"
)

var db *sql.DB

// DatabaseInstance returns a database instance
func DatabaseInstance() (*sql.DB, error) {
	var err error

	if db != nil {
		return db, nil
	}

	database := GetEnv("DB_NAME", "app")
	user := GetEnv("DB_USER", "root")
	password := GetEnv("DB_PASSWORD", "root")
	mode := GetEnv("DB_MODE", "disable")
	host := GetEnv("DB_HOST", "localhost")

	connectionString := fmt.Sprintf(
		"postgres://%s:%s@%s/%s?sslmode=%s",
		user, password, host, database, mode,
	)
	db, err = sql.Open("postgres", connectionString)

	return db, err
}
